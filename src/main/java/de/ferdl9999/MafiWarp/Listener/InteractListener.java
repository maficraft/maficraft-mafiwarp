package de.ferdl9999.MafiWarp.Listener;

import de.ferdl9999.MafiWarp.MafiWarp;
import de.ferdl9999.MafiWarp.Utils.WarpCache;
import de.ferdl9999.MafiWarp.Utils.WarpsManager;
import de.ferdl9999.MafiWarp.WarpEvent;
import net.milkbowl.vault.economy.Economy;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.player.PlayerInteractEvent;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.UUID;
import java.util.logging.Level;
import java.util.logging.Logger;

public class InteractListener implements Listener {

    private final HashMap<UUID, Long> currentlyWalked = new HashMap<>();

    private final long RESET_TIME = 36000000;
    private final double REWARD_MULTIPLIER = 0.9D;

    @EventHandler
    public void onInteract(PlayerInteractEvent evt) {
        if (evt.getAction() == Action.PHYSICAL) {
            if (!evt.getPlayer().isDead()) {
                if (evt.getPlayer().getHealth() != 0.0D) {
                    if ((evt.getClickedBlock().getType() == Material.STONE_PLATE) || (evt.getClickedBlock().getType() == Material.WOOD_PLATE)) {
                        if (WarpCache.warpCache.containsKey(evt.getClickedBlock().getLocation())) {
                            runIt(evt.getPlayer(), evt);
                        }
                    }
                }
            }
        } else if ((evt.getAction() == Action.RIGHT_CLICK_BLOCK) && ((evt.getClickedBlock().getType() == Material.WOOD_BUTTON) || (evt.getClickedBlock().getType() == Material.STONE_BUTTON))) {
            if (WarpCache.warpCache.containsKey(evt.getClickedBlock().getLocation())) {
                runIt(evt.getPlayer(), evt);
            }
        }
    }

    private void runIt(final Player p, final PlayerInteractEvent evt) {
        if (currentlyWalked.containsKey(p.getUniqueId())) {
            if ((System.currentTimeMillis() - currentlyWalked.get(p.getUniqueId()) <= 3000)) {
                return;
            }
        }
        currentlyWalked.put(p.getUniqueId(), System.currentTimeMillis());

        Bukkit.getScheduler().runTaskAsynchronously(MafiWarp.getInstance(), () -> {
            final String buttonName = WarpCache.warpCache.get(evt.getClickedBlock().getLocation());

            String msg = WarpsManager.getMsg(buttonName);
            final Location toWarp = WarpsManager.getLoc(buttonName);
            final int reward = getReward(p, WarpsManager.getReward(buttonName), buttonName);
            int cost = WarpsManager.getCost(buttonName);
            if (!WarpsManager.canUse(p, buttonName)) {
                p.sendMessage("§cDu kannst diesen Button/diese Druckplatte nicht nochmal nutzen");
                evt.setCancelled(true);

                currentlyWalked.remove(p.getUniqueId());
                return;
            }

            Economy economy = MafiWarp.getInstance().getEconomy();
            if (!economy.has(p, cost)) {
                p.sendMessage("§cDu hast nicht genug Geld");
                evt.setCancelled(true);

                currentlyWalked.remove(p.getUniqueId());
                return;
            }
            runCmd(p, buttonName);

            if (msg != null) {
                p.sendMessage(ChatColor.translateAlternateColorCodes('&', msg));
            }
//            if (reward != 0) {
//                economy.depositPlayer(p, reward);
//            }
            if (cost != 0) {
                economy.withdrawPlayer(p, cost);
            }

            addTime(p, buttonName);

            Bukkit.getScheduler().scheduleSyncDelayedTask(MafiWarp.getInstance(), () -> {
                p.teleport(toWarp);

                WarpEvent event = new WarpEvent(p, buttonName, reward);

                Bukkit.getPluginManager().callEvent(event);

                currentlyWalked.remove(p.getUniqueId());
            });

            saveReward();

        });
    }

    private int getReward(Player p, int actualReward, String mwName) {
        if (actualReward > 10) {
            if (p.getWorld().getName().equalsIgnoreCase("eventworld")) {
                long resetTime = RESET_TIME;
                if (MafiWarp.getInstance().warpsConfig.contains("warps." + mwName + ".time")) {
                    resetTime = MafiWarp.getInstance().warpsConfig.getInt("warps." + mwName + ".time") * 1000;
                }

                List<String> times;
                if (MafiWarp.getInstance().usetimesConfig.contains("times." + p.getUniqueId().toString())) {
                    times = MafiWarp.getInstance().usetimesConfig.getStringList("times." + p.getUniqueId().toString());
                } else {
                    times = new ArrayList<>();
                }
                List<String> toRemove = new ArrayList<>();

                for (String s : times) {
                    if ((Long.parseLong(s.split(";;")[1]) + resetTime) <= System.currentTimeMillis()) {
                        toRemove.add(s);
                    }
                }
                for (String s : toRemove) {
                    times.remove(s);
                }

                int actCount = 0;

                for (String s : times) {
                    if (s.split(";;")[0].equals(mwName)) {
                        actCount++;
                    }
                }

                int curReward = actualReward;

                for (int cur = 0; cur < actCount; cur++) {
                    curReward = (int) (REWARD_MULTIPLIER * curReward);
                }

                MafiWarp.getInstance().usetimesConfig.set("times." + p.getUniqueId().toString(), times);

                return Math.max(curReward, 1);

            }
        }
        return actualReward;
    }

    private void addTime(Player p, String mw) {
        List<String> times;
        if (MafiWarp.getInstance().usetimesConfig.contains("times." + p.getUniqueId().toString())) {
            times = MafiWarp.getInstance().usetimesConfig.getStringList("times." + p.getUniqueId().toString());
        } else {
            times = new ArrayList<>();
        }

        times.add(mw + ";;" + System.currentTimeMillis());

        MafiWarp.getInstance().usetimesConfig.set("times." + p.getUniqueId().toString(), times);
    }

    private void saveReward() {
        try {
            MafiWarp.getInstance().usetimesConfig.save(MafiWarp.getInstance().usetimes);
        } catch (IOException ex) {
            Logger.getLogger(InteractListener.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    private void runCmd(Player p, String warpName) {
        if (WarpsManager.getCmd(warpName).equalsIgnoreCase("")) {
            return;
        }
        String cmd_ = WarpsManager.getCmd(warpName);
        String executor = WarpsManager.getExecutor(warpName);

        String cmd = cmd_.replaceAll("%p", p.getName());
        if (executor.equalsIgnoreCase("console")) {
            Bukkit.dispatchCommand(Bukkit.getConsoleSender(), cmd);
        } else {
            try {
                Bukkit.dispatchCommand(p, cmd);
            } catch (Exception e) {
                MafiWarp.getPlugin(MafiWarp.class).getLogger().log(Level.WARNING, "Fehler beim Ausführen des Befehls", e);
            }
        }
    }
}
