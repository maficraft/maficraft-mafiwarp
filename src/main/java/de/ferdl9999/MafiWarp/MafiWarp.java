package de.ferdl9999.MafiWarp;

import de.ferdl9999.MafiWarp.Commands.MafiWarpCommand;
import de.ferdl9999.MafiWarp.Listener.InteractListener;
import de.ferdl9999.MafiWarp.Utils.WarpCache;
import net.milkbowl.vault.economy.Economy;
import net.milkbowl.vault.permission.Permission;
import org.bukkit.Bukkit;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.plugin.RegisteredServiceProvider;
import org.bukkit.plugin.java.JavaPlugin;

import java.io.File;

public class MafiWarp
        extends JavaPlugin {

    private static MafiWarp instance;
    private static final String prefix = "§6MafiWarp §8» §r";
    public File warps;
    public YamlConfiguration warpsConfig;
    public File buttons;
    public YamlConfiguration buttonsConfig;
    public File used;
    public YamlConfiguration usedConfig;

    public File usetimes;
    public YamlConfiguration usetimesConfig;

    private MafiWarpCommand maficmd;
    private InteractListener interactlistener;
    private WarpCache warpcache;
    private Economy economy;
    private Permission permission;

    public static MafiWarp getInstance() {
        return instance;
    }

    public static String getPrefix() {
        return prefix;
    }

    public Economy getEconomy() {
        return economy;
    }

    public Permission getPermission() {
        return permission;
    }

    @Override
    public void onEnable() {
        initVariables();
        initFiles();
        setupEconomy();
        setupPermission();
        initCommands();

        Thread cache = new Thread(this.warpcache, "MafiWarp-Cache-Thread");

        cache.start();
    }

    @Override
    public void onDisable() {
        warpcache.setEnabled(false);
    }

    private void setupPermission() {
        final RegisteredServiceProvider<Permission> pProv = Bukkit.getServicesManager().getRegistration(Permission.class);
        permission = pProv.getProvider();
    }

    private void setupEconomy() {
        RegisteredServiceProvider<Economy> eProv = Bukkit.getServicesManager().getRegistration(Economy.class);
        economy = eProv.getProvider();
    }

    private void initVariables() {
        instance = this;
        this.maficmd = new MafiWarpCommand();
        this.warpcache = new WarpCache();
        this.interactlistener = new InteractListener();

        getServer().getPluginManager().registerEvents(this.interactlistener, this);
    }

    private void initFiles() {
        if (!getDataFolder().exists()) {
            getDataFolder().mkdirs();
        }
        this.warps = new File(getDataFolder(), "warps.yml");
        this.warpsConfig = YamlConfiguration.loadConfiguration(this.warps);

        this.buttons = new File(getDataFolder(), "buttons.yml");
        this.buttonsConfig = YamlConfiguration.loadConfiguration(this.buttons);

        this.used = new File(getDataFolder(), "used.yml");
        this.usedConfig = YamlConfiguration.loadConfiguration(this.used);

        this.usetimes = new File(getDataFolder(), "usetimes.yml");
        this.usetimesConfig = YamlConfiguration.loadConfiguration(this.usetimes);
    }

    private void initCommands() {
        getCommand("mafiwarp").setExecutor(this.maficmd);
    }

}
