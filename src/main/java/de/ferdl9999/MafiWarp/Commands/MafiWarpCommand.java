package de.ferdl9999.MafiWarp.Commands;

import de.ferdl9999.MafiWarp.MafiWarp;
import de.ferdl9999.MafiWarp.Utils.WarpsManager;
import net.milkbowl.vault.permission.Permission;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class MafiWarpCommand implements CommandExecutor {

    @Override
    public boolean onCommand(CommandSender sender, Command cmd, String string, String[] args) {
        if (!(sender instanceof Player)) {
            return true;
        }
        Player p = (Player) sender;
        if (!p.hasPermission("mafiwarp.mod")) {
            p.sendMessage(MafiWarp.getPrefix() + "§cKeine Berechtigungen");
            return true;
        }

        if (args.length == 0) {
            p.sendMessage(MafiWarp.getPrefix() + "§cUnbekannter Befehl. Bitte §e/mw help §ceingeben!");
            return true;
        }

        if (args.length == 1) {
            if (args[0].equalsIgnoreCase("info")) {
                if (!WarpsManager.searchforButton(p)) {
                    p.sendMessage(MafiWarp.getPrefix() + "§eDer Block, den du anschaust, ist kein Button.");
                }
            } else if (args[0].equalsIgnoreCase("help")) {
                sendHelpMessage(sender, 1);
            } else {
                teleportToWarp(args[0], p);
            }
            return true;
        } else if (args.length == 2) {
            if (args[0].equalsIgnoreCase("help")) {
                if (!isInteger(args[1])) {
                    p.sendMessage(MafiWarp.getPrefix() + "§e'" + args[1] + "' $cist keine Zahl!");
                }
                sendHelpMessage(sender, Integer.parseInt(args[1]));
                return true;
            } else if ((args[0].equalsIgnoreCase("make")) || (args[0].equalsIgnoreCase("create"))) {
                WarpsManager.createWarp(args[1], p.getLocation());
                p.sendMessage(MafiWarp.getPrefix() + "§eWarp §a" + args[1] + " §eerfolgreich erstellt.");
                p.sendMessage(MafiWarp.getPrefix() + "§3Position:");
                p.sendMessage("               §cWelt: §b" + p.getLocation().getWorld().getName());
                p.sendMessage("               §cX: §b" + p.getLocation().getBlockX());
                p.sendMessage("               §cY: §b" + p.getLocation().getBlockY());
                p.sendMessage("               §cZ: §b" + p.getLocation().getBlockZ());
                p.sendMessage("               §cYaw: §b" + p.getLocation().getYaw());
                p.sendMessage("               §cPitch: §b" + p.getLocation().getPitch());
            } else if ((args[0].equalsIgnoreCase("delete")) || (args[0].equalsIgnoreCase("remove"))) {
                if (WarpsManager.exists(args[1])) {
                    WarpsManager.delete(args[1]);
                    p.sendMessage(MafiWarp.getPrefix() + "§eDu hast den Warp §a" + args[1] + " §egelöscht.");
                    return true;
                }
                p.sendMessage(MafiWarp.getPrefix() + "§cWarp §e" + args[1] + "§r §cexistiert nicht.");
                return true;
            } else if (args[0].equalsIgnoreCase("link")) {
                if (!WarpsManager.exists(args[1])) {
                    p.sendMessage(MafiWarp.getPrefix() + "§cWarp §e" + args[1] + "§r §cexistiert nicht.");
                    return true;
                }
                if (!WarpsManager.linkWarp(p, args[1])) {
                    p.sendMessage(MafiWarp.getPrefix() + "§cDu musst eine Druckplatte oder einen Button anschauen!");
                }
            } else if (args[0].equalsIgnoreCase("unlink")) {
                if (!WarpsManager.exists(args[1])) {
                    p.sendMessage(MafiWarp.getPrefix() + "§cWarp §e" + args[1] + "§r §cexistiert nicht.");
                    return true;
                }
                if (!WarpsManager.unlinkWarp(p, args[1])) {
                    p.sendMessage(MafiWarp.getPrefix() + "§cEs gibt keinen Button mit dem Namen §e" + args[1]);
                }
            } else if (args[0].equalsIgnoreCase("info")) {
                if (!WarpsManager.exists(args[1])) {
                    p.sendMessage(MafiWarp.getPrefix() + "§cWarp §e" + args[1] + "§r §cexistiert nicht.");
                } else {
                    WarpsManager.sendInfo(p, args[1]);
                }
            } else if ((args[0].equalsIgnoreCase("teleport")) || (args[0].equalsIgnoreCase("tp"))) {
                teleportToWarp(args[1], p);
            }
        } else if (args.length == 3) {
            if (args[0].equalsIgnoreCase("time")) {
                if (isInteger(args[2])) {
                    if (!WarpsManager.exists(args[1])) {
                        p.sendMessage(MafiWarp.getPrefix() + "§cWarp §e" + args[1] + "§r §cexistiert nicht.");
                        return true;
                    }
                    int time = Integer.parseInt(args[2]);
                    WarpsManager.setTime(args[1], time);
                    p.sendMessage(MafiWarp.getPrefix() + "§cReset-Zeit von §e" + args[1] + "§r §cauf §e" + time + " Sekunden§r §cgesetzt.");

                    return true;
                } else if (args[2].equalsIgnoreCase("default")) {
                    WarpsManager.setTime(args[1], -1);
                    p.sendMessage(MafiWarp.getPrefix() + "§cReset-Zeit von §e" + args[1] + "§r §cauf §eDEFAULT§c gesetzt.");
                    return true;
                }
                p.sendMessage(MafiWarp.getPrefix() + "§b" + args[2] + "§r §cist keine Zahl.");
                return true;
            }
            if (args[0].equalsIgnoreCase("reward")) {
                if (isInteger(args[2])) {
                    if (!WarpsManager.exists(args[1])) {
                        p.sendMessage(MafiWarp.getPrefix() + "§cWarp §e" + args[1] + "§r §cexistiert nicht.");
                        return true;
                    }
                    int reward = Integer.parseInt(args[2]);
//                    if (checkReward(p, reward)) {
                    WarpsManager.setReward(args[1], reward);
                    p.sendMessage(MafiWarp.getPrefix() + "§cBelohnung von §e" + args[1] + "§r §cauf §e" + reward + " Mafis§r §cgesetzt.");
//                    }
                    return true;
                }
                p.sendMessage(MafiWarp.getPrefix() + "§b" + args[2] + "§r §cist keine Zahl");
                return true;
            }
            if (args[0].equalsIgnoreCase("cost")) {
                if (isInteger(args[2])) {
                    if (!WarpsManager.exists(args[1])) {
                        p.sendMessage(MafiWarp.getPrefix() + "§cWarp §e" + args[1] + "§r §cexistiert nicht");
                        return true;
                    }
                    int cost = Integer.parseInt(args[2]);
//                    if (checkReward(p, cost)) {
                    WarpsManager.setCost(args[1], cost);
                    p.sendMessage(MafiWarp.getPrefix() + "§cPreis von §e" + args[1] + "§r §cauf §e" + cost + " Mafis§r §cgesetzt");
//                    }
                    return true;
                }
                p.sendMessage(MafiWarp.getPrefix() + "§b" + args[2] + "§r §cist keine Zahl");
                return true;
            }
            if (args[0].equalsIgnoreCase("msg")) {
                if (WarpsManager.exists(args[1])) {
                    String msg = args[2];
                    WarpsManager.setMsg(args[1], msg);

                    String msg_trans = ChatColor.translateAlternateColorCodes('&', msg);

                    p.sendMessage(MafiWarp.getPrefix() + "§aNachricht von §e" + args[1] + "§a lautet nun: §f" + msg_trans);
                    return true;
                }
                p.sendMessage(MafiWarp.getPrefix() + "§cDieser MafiWarp existiert nicht!");
                return true;
            }
            if (args[0].equalsIgnoreCase("cmd-executor")) {
                if (!p.hasPermission("mafiwarp.mod")) {
                    p.sendMessage(MafiWarp.getPrefix() + "§cKeine Berechtigungen");
                    return true;
                }
                String warp = args[1];
                if (!WarpsManager.exists(warp)) {
                    p.sendMessage(MafiWarp.getPrefix() + "§cDieser MafiWarp existiert nicht!");
                    return true;
                }
                if (args[2].equalsIgnoreCase("player")) {
                    if (!WarpsManager.setExecutor(warp, "player")) {
                        p.sendMessage(MafiWarp.getPrefix() + "§cDieser MafiWarp existiert nicht!");
                        return true;
                    }
                    p.sendMessage(MafiWarp.getPrefix() + "§eCommand-Executor des Warps §a" + warp + ": §bplayer");
                    return true;
                }
                if (args[2].equalsIgnoreCase("console")) {
                    if (!WarpsManager.setExecutor(warp, "console")) {
                        p.sendMessage(MafiWarp.getPrefix() + "§cDieser MafiWarp existiert nicht!");
                        return true;
                    }
                    p.sendMessage(MafiWarp.getPrefix() + "§eCommand-Executor des Warps §a" + warp + ": §bconsole");
                    return true;
                }
                p.sendMessage(MafiWarp.getPrefix() + "§cDieser Command-Executor existiert nicht!");
                return true;
            }
            if (args[0].equalsIgnoreCase("cmd")) {
                if (!p.hasPermission("mafiwarp.admin")) {
                    p.sendMessage(MafiWarp.getPrefix() + "§cKeine Berechtigungen");
                    return true;
                }
                String warp = args[1];
                if (!WarpsManager.exists(warp)) {
                    p.sendMessage(MafiWarp.getPrefix() + "§cDieser MafiWarp existiert nicht!");
                    return true;
                }
                if (!WarpsManager.setCmd(warp, args[2])) {
                    p.sendMessage(MafiWarp.getPrefix() + "§cDieser MafiWarp existiert nicht!");
                    return true;
                }
                p.sendMessage(MafiWarp.getPrefix() + "§eBefehl des Warps §a" + warp + ": §b" + args[2]);
                return true;
            }
            if (args[0].equalsIgnoreCase("useonce")) {
                if (!WarpsManager.exists(args[1])) {
                    p.sendMessage(MafiWarp.getPrefix() + "§cWarp §e" + args[1] + "§r §cexistiert nicht");
                    return true;
                }
                if ((args[2].equalsIgnoreCase("true")) || (args[2].equalsIgnoreCase("false"))) {
                    if (args[2].equalsIgnoreCase("true")) {
                        if (!WarpsManager.setUseOnce(args[1], true)) {
                            p.sendMessage(MafiWarp.getPrefix() + "§cWarp §e" + args[1] + "§r §cexistiert nicht");
                            return true;
                        }
                        p.sendMessage(MafiWarp.getPrefix() + "§eEinmalige Benutzung des Warps §a" + args[1] + ": §bAktiviert");
                        return true;
                    }
                    if (!WarpsManager.setUseOnce(args[1], false)) {
                        p.sendMessage(MafiWarp.getPrefix() + "§cWarp §e" + args[1] + "§r §cexistiert nicht");
                        return true;
                    }
                    p.sendMessage(MafiWarp.getPrefix() + "§eEinmalige Benutzung des Warps §a" + args[1] + ": §bDeaktiviert");
                    return true;
                }
                p.sendMessage(MafiWarp.getPrefix() + "§cFalsche Eingabe des Befehls!");
                p.sendMessage(MafiWarp.getPrefix() + "§cDu musst statt §e" + args[2] + " §btrue §coder §bfalse §ceingeben");
                return true;
            }
        } else {
            if (args[0].equalsIgnoreCase("msg")) {
                if (WarpsManager.exists(args[1])) {
                    StringBuilder msg = new StringBuilder();
                    for (int i = 2; i < args.length; i++) {
                        msg.append(args[i]).append(" ");
                    }
                    WarpsManager.setMsg(args[1], msg.toString());

                    String msg_trans = ChatColor.translateAlternateColorCodes('&', msg.toString());

                    p.sendMessage(MafiWarp.getPrefix() + "§aNachricht von §e" + args[1] + "§a lautet nun: §f" + msg_trans);
                    return true;
                }
                p.sendMessage(MafiWarp.getPrefix() + "§cDieser MafiWarp existiert nicht!");
                return true;
            }
            if (args[0].equalsIgnoreCase("cmd")) {
                if (!p.hasPermission("mafiwarp.admin")) {
                    p.sendMessage(MafiWarp.getPrefix() + "§cKeine Berechtigungen");
                    return true;
                }
                String warp = args[1];
                if (!WarpsManager.exists(warp)) {
                    p.sendMessage(MafiWarp.getPrefix() + "§cDieser MafiWarp existiert nicht!");
                    return true;
                }
                StringBuilder commandBuilder = new StringBuilder();
                for (int i = 2; i < args.length; i++) {
                    commandBuilder.append(args[i]).append(" ");
                }
                String command = commandBuilder.toString();
                if (!WarpsManager.setCmd(warp, command)) {
                    p.sendMessage(MafiWarp.getPrefix() + "§cDieser MafiWarp existiert nicht!");
                    return true;
                }
                p.sendMessage(MafiWarp.getPrefix() + "§eBefehl des Warps §a" + warp + ": §b" + command);
                return true;
            } else {
                p.sendMessage(MafiWarp.getPrefix() + "§cUnbekannter Befehl. Bitte §e/mw help §ceingeben!");
            }
        }
        return true;
    }

    private void teleportToWarp(String warp, Player player) {
        if (WarpsManager.exists(warp)) {
            player.teleport(WarpsManager.getLoc(warp));
            player.sendMessage(MafiWarp.getPrefix() + "§eDu wurdest zum Warp §a" + warp + " §eteleportiert.");
        } else {
            player.sendMessage(MafiWarp.getPrefix() + "§cWarp §e" + warp + "§r §cexistiert nicht.");
        }
    }

    private void sendHelpMessage(CommandSender sender, int page) {
        sender.sendMessage(MafiWarp.getPrefix() + "§6[]=======> §eHilfeseite (" + page + "/3) §6<=======[]");

        if (page == 1) {
            sender.sendMessage(MafiWarp.getPrefix() + "§e/mw help [Seite]§7 - Hilfe für MafiWarp aufrufen");
            sender.sendMessage(MafiWarp.getPrefix() + "§e/mw <Name> | /mw tp <Name> §7 - Sich zu MafiWarp teleportieren");
            sender.sendMessage(MafiWarp.getPrefix() + "§e/mw create <Name>§7 - MafiWarp erstellen");
            sender.sendMessage(MafiWarp.getPrefix() + "§e/mw remove <Name>§7 - MafiWarp löschen");
            sender.sendMessage(MafiWarp.getPrefix() + "§e/mw link <Name>§7 - Angeschauter Button oder angeschaute Druckplatte mit MafiWarp verlinken");
        } else if (page == 2) {
            sender.sendMessage(MafiWarp.getPrefix() + "§e/mw unlink <Name>§7 - Verlinkung eines Buttons oder Druckplatte entfernen");
            sender.sendMessage(MafiWarp.getPrefix() + "§e/mw reward <Name>§7 - Belohnung für den MafiWarp festlegen");
            sender.sendMessage(MafiWarp.getPrefix() + "§e/mw cost <Name>§7 - Kosten für den MafiWarp festlegen");
            sender.sendMessage(MafiWarp.getPrefix() + "§e/mw msg <Name> <Nachricht>§7 - Nachricht für MafiWarp festlegen");
        } else if (page == 3) {
            sender.sendMessage(MafiWarp.getPrefix() + "§e/mw cmd <Name> <Befehl>§7 - Befehl bei MafiWarp-Betätigung ausführen");
            sender.sendMessage(MafiWarp.getPrefix() + "§e/mw cmd-executor <player/console>§7 - Ob der verlinkte Befehl vom Spieler oder von der Console ausgeführt werden soll");
            sender.sendMessage(MafiWarp.getPrefix() + "§e/mw useonce <Name> <true/false>§7 - Ob der MafiWarp nur einmal verwendet werden darf");
        }

    }

    private boolean checkReward(Player p, int reward) {
        // TODO rewrite reward check entirely
        if (p.getName().equalsIgnoreCase("ferdl9999") || p.getName().equalsIgnoreCase("MaFiMa")) {
            return true;
        }

        Permission permission = MafiWarp.getInstance().getPermission();

        if (permission.getPrimaryGroup(p).equalsIgnoreCase("xModerator")) {
            if (reward >= 50000) {
                Bukkit.dispatchCommand(Bukkit.getConsoleSender(), "ban " + p.getName() + " [MW-Ban] Bitte im TS bei einem Admin melden");
                return false;
            } else if (reward >= 10000) {
                p.sendMessage("§7[MafiWarp] §aMelde dich bei einem Admin, um einen solchen Betrag zu setzen");
                return false;
            }
        } else if (permission.getPrimaryGroup(p).equalsIgnoreCase("Architekt")) {
            if (reward >= 50000) {
                Bukkit.dispatchCommand(Bukkit.getConsoleSender(), "ban " + p.getName() + " [MW-Ban] Bitte im TS bei einem Admin melden");
                return false;
            } else if (reward >= 10000) {
                p.sendMessage("§7[MafiWarp] §aMelde dich bei einem Admin, um einen solchen Betrag zu setzen");
                return false;
            }
        } else if (permission.getPrimaryGroup(p).equalsIgnoreCase("xCoAdmin")) {
            if (reward >= 70000) {
                Bukkit.dispatchCommand(Bukkit.getConsoleSender(), "ban " + p.getName() + " [MW-Ban] Bitte im TS bei einem Admin melden");
                return false;
            } else if (reward >= 30000) {
                p.sendMessage("§7[MafiWarp] §aMelde dich bei einem Admin, um einen solchen Betrag zu setzen");
                return false;
            }
        } else if (permission.getPrimaryGroup(p).equalsIgnoreCase("xDeveloper")) {
            if (reward >= 70000) {
                Bukkit.dispatchCommand(Bukkit.getConsoleSender(), "ban " + p.getName() + " [MW-Ban] Bitte im TS bei einem Admin melden");
                return false;
            } else if (reward >= 40000) {
                p.sendMessage("§7[MafiWarp] §aMelde dich bei einem Admin, um einen solchen Betrag zu setzen");
                return false;
            }
        } else if (permission.getPrimaryGroup(p).equalsIgnoreCase("xAdmin")) {
            if (reward >= 100000) {
                Bukkit.dispatchCommand(Bukkit.getConsoleSender(), "ban " + p.getName() + " [MW-Ban] Bitte im TS bei einem Admin melden");
                return false;
            } else if (reward >= 50000) {
                p.sendMessage("§7[MafiWarp] §aMelde dich bei Ferdl oder Mafi, um einen solchen Betrag zu setzen");
                return false;
            }
        }
        if (reward >= 100000) {
            Bukkit.dispatchCommand(Bukkit.getConsoleSender(), "ban " + p.getName() + " [MW-Ban] Bitte im TS bei einem Admin melden");
            return false;
        } else if (reward >= 50000) {
            p.sendMessage("§7[MafiWarp] §aMelde dich bei Ferdl oder Mafi, um einen solchen Betrag zu setzen");
            return false;
        }

        return true;
    }

    private static boolean isInteger(String s) {
        try {
            Integer.parseInt(s);
        } catch (NumberFormatException e) {
            return false;
        }
        return true;
    }
}
