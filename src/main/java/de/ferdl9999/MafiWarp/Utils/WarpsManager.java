package de.ferdl9999.MafiWarp.Utils;

import de.ferdl9999.MafiWarp.MafiWarp;
import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.World;
import org.bukkit.block.Block;
import org.bukkit.entity.Player;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;

public class WarpsManager {

    private static final MafiWarp main = MafiWarp.getInstance();

    public static boolean createWarp(String name, Location loc) {
        if (main.warpsConfig.contains("warps." + name)) {
            main.warpsConfig.set("warps." + name + ".loc.world", loc.getWorld().getName());
            main.warpsConfig.set("warps." + name + ".loc.x", loc.getX());
            main.warpsConfig.set("warps." + name + ".loc.y", loc.getY());
            main.warpsConfig.set("warps." + name + ".loc.z", loc.getZ());
            main.warpsConfig.set("warps." + name + ".loc.yaw", loc.getYaw());
            main.warpsConfig.set("warps." + name + ".loc.pitch", loc.getPitch());

            saveWarps();
            return false;
        }
        main.warpsConfig.set("warps." + name + ".loc.world", loc.getWorld().getName());
        main.warpsConfig.set("warps." + name + ".loc.x", loc.getX());
        main.warpsConfig.set("warps." + name + ".loc.y", loc.getY());
        main.warpsConfig.set("warps." + name + ".loc.z", loc.getZ());
        main.warpsConfig.set("warps." + name + ".loc.yaw", loc.getYaw());
        main.warpsConfig.set("warps." + name + ".loc.pitch", loc.getPitch());
        main.warpsConfig.set("warps." + name + ".reward", 0);
        main.warpsConfig.set("warps." + name + ".cost", 0);
        main.warpsConfig.set("warps." + name + ".msg", "");
        main.warpsConfig.set("warps." + name + ".cmd", "");
        main.warpsConfig.set("warps." + name + ".cmd-executor", "player");
        main.warpsConfig.set("warps." + name + ".useonce", "false");
        saveWarps();

        return true;
    }


    public static String getCmd(String name) {
        String cmd = null;
        if (main.warpsConfig.contains("warps." + name)) {
            cmd = main.warpsConfig.getString("warps." + name + ".cmd");
        }
        return cmd;
    }

    public static boolean setCmd(String name, String cmd) {
        if (!main.warpsConfig.contains("warps." + name)) {
            return false;
        }
        main.warpsConfig.set("warps." + name + ".cmd", cmd);

        saveWarps();

        return true;
    }

    public static String getExecutor(String name) {
        String executor = null;
        if (main.warpsConfig.contains("warps." + name)) {
            executor = main.warpsConfig.getString("warps." + name + ".cmd-executor");
        }
        return executor;
    }

    public static boolean setExecutor(String name, String executor) {
        if (!main.warpsConfig.contains("warps." + name)) {
            return false;
        }
        main.warpsConfig.set("warps." + name + ".cmd-executor", executor);

        saveWarps();

        return true;
    }

    public static String getMsg(String name) {
        String msg = null;
        if (main.warpsConfig.contains("warps." + name)) {
            String msg_ = main.warpsConfig.getString("warps." + name + ".msg");
            if (!msg_.equals("")) {
                msg = msg_;
            }
        }
        return msg;
    }

    public static boolean setMsg(String name, String msg) {
        if (!main.warpsConfig.contains("warps." + name)) {
            return false;
        }
        main.warpsConfig.set("warps." + name + ".msg", msg);

        saveWarps();

        return true;
    }

    public static boolean setReward(String name, int reward) {
        if (!main.warpsConfig.contains("warps." + name)) {
            return false;
        }
        main.warpsConfig.set("warps." + name + ".reward", reward);

        saveWarps();

        return true;
    }

    public static boolean setTime(String name, int time) {
        if (!main.warpsConfig.contains("warps." + name)) {
            return false;
        }
        if (time > 0) {
            main.warpsConfig.set("warps." + name + ".time", time);
        } else {
            main.warpsConfig.set("warps." + name + ".time", null);
        }

        saveWarps();

        return true;
    }

    public static boolean setCost(String name, int cost) {
        if (!main.warpsConfig.contains("warps." + name)) {
            return false;
        }
        main.warpsConfig.set("warps." + name + ".cost", cost);

        saveWarps();

        return true;
    }

    public static Location getLoc(String name) {
        Location loc = null;
        if (main.warpsConfig.contains("warps." + name + ".loc")) {
            World world = Bukkit.getWorld(main.warpsConfig.getString("warps." + name + ".loc.world"));

            double x;
            double y;
            double z;
            try {
                x = main.warpsConfig.getDouble("warps." + name + ".loc.x");
                y = main.warpsConfig.getDouble("warps." + name + ".loc.y");
                z = main.warpsConfig.getDouble("warps." + name + ".loc.z");
            } catch (Exception e) {
                x = main.warpsConfig.getInt("warps." + name + ".loc.x");
                y = main.warpsConfig.getInt("warps." + name + ".loc.y");
                z = main.warpsConfig.getInt("warps." + name + ".loc.z");
            }

            float yaw = Float.parseFloat(main.warpsConfig.getString("warps." + name + ".loc.yaw"));
            float pitch = Float.parseFloat(main.warpsConfig.getString("warps." + name + ".loc.pitch"));

            loc = new Location(world, x, y, z, yaw, pitch);
        }
        return loc;
    }

    public static int getReward(String name) {
        int reward = 0;
        if (main.warpsConfig.contains("warps." + name + ".reward")) {
            reward = main.warpsConfig.getInt("warps." + name + ".reward");
        }
        return reward;
    }

    public static int getCost(String name) {
        int cost = 0;
        if (main.warpsConfig.contains("warps." + name + ".cost")) {
            cost = main.warpsConfig.getInt("warps." + name + ".cost");
        }
        return cost;
    }

    public static boolean exists(String name) {
        return main.warpsConfig.contains("warps." + name);
    }

    public static void delete(String name) {
        main.warpsConfig.set("warps." + name, null);
        saveWarps();
        if (main.buttonsConfig.contains("buttons." + name)) {
            main.buttonsConfig.set("buttons." + name, null);
            try {
                main.buttonsConfig.save(main.buttons);
            } catch (IOException ex) {
                Logger.getLogger(WarpsManager.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        if (main.usedConfig.contains("players." + name)) {
            main.usedConfig.set("players." + name, null);
            try {
                main.usedConfig.save(main.used);
            } catch (IOException ex) {
                Logger.getLogger(WarpsManager.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }

    public static boolean searchforButton(Player p) {
        Set<Material> invisible = new HashSet<>();
        invisible.add(Material.AIR);
        Block b = p.getTargetBlock(invisible, 50);
        if ((b.getType() == Material.STONE_PLATE) || (b.getType() == Material.WOOD_PLATE) || (b.getType() == Material.WOOD_BUTTON) || (b.getType() == Material.STONE_BUTTON)) {
            if (WarpCache.warpCache.containsKey(b.getLocation())) {
                String buttonName = WarpCache.warpCache.get(b.getLocation());

                sendInfo(p, buttonName);

                return true;
            }
            p.sendMessage(MafiWarp.getPrefix() + "§cDieser Button ist nicht verlinkt");

            return true;
        }
        return false;
    }

    public static void sendInfo(Player p, String warp) {
        p.sendMessage("§aName: §f" + warp);
        p.sendMessage("§aMsg: " + getMsg(warp));
        p.sendMessage("§aGewinn: " + getReward(warp));
        p.sendMessage("§aKosten: " + getCost(warp));
    }

    public static boolean linkWarp(Player p, String warp) {
        Set<Material> invisible = new HashSet<>();
        invisible.add(Material.AIR);
        Block b = p.getTargetBlock(invisible, 50);
        if ((b.getType() == Material.STONE_PLATE) || (b.getType() == Material.WOOD_PLATE) || (b.getType() == Material.WOOD_BUTTON) || (b.getType() == Material.STONE_BUTTON)) {
            List<String> locs;
            if (MafiWarp.getInstance().buttonsConfig.contains("buttons." + warp)) {
                locs = MafiWarp.getInstance().buttonsConfig.getStringList("buttons." + warp);
            } else {
                locs = new ArrayList<>();
            }
            String loc = b.getWorld().getName() + "," + b.getX() + "," + b.getY() + "," + b.getZ();
            if (!locs.contains(loc)) {
                locs.add(loc);
            } else {
                p.sendMessage(MafiWarp.getPrefix() + "§cDieser Button ist schon verlinkt");
                return true;
            }
            MafiWarp.getInstance().buttonsConfig.set("buttons." + warp, locs);
            try {
                MafiWarp.getInstance().buttonsConfig.save(MafiWarp.getInstance().buttons);
            } catch (IOException ex) {
                Logger.getLogger(WarpsManager.class.getName()).log(Level.SEVERE, null, ex);
            }
            p.sendMessage(MafiWarp.getPrefix() + "§cButton verlinkt");

            return true;
        }
        return false;
    }

    public static boolean unlinkWarp(Player p, String warp) {
        if (main.buttonsConfig.contains("buttons." + warp)) {
            Set<Material> invisible = new HashSet<>();
            invisible.add(Material.AIR);
            Block b = p.getTargetBlock(invisible, 50);
            if ((b.getType() == Material.STONE_PLATE) || (b.getType() == Material.WOOD_PLATE) || (b.getType() == Material.WOOD_BUTTON) || (b.getType() == Material.STONE_BUTTON)) {
                List<String> locs = MafiWarp.getInstance().buttonsConfig.getStringList("buttons." + warp);

                String loc = b.getWorld().getName() + "," + b.getX() + "," + b.getY() + "," + b.getZ();
                if (locs.contains(loc)) {
                    locs.remove(loc);
                } else {
                    p.sendMessage(MafiWarp.getPrefix() + "§cDer Button den du anschaust ist nicht verlinkt");
                    return true;
                }
                MafiWarp.getInstance().buttonsConfig.set("buttons." + warp, locs);
                try {
                    MafiWarp.getInstance().buttonsConfig.save(MafiWarp.getInstance().buttons);
                } catch (IOException ex) {
                    Logger.getLogger(WarpsManager.class.getName()).log(Level.SEVERE, null, ex);
                }
                p.sendMessage(MafiWarp.getPrefix() + "§cDer Button ist nichtmehr verlinkt!");

                return true;
            }
            return false;
        }
        return false;
    }

    public static boolean setUseOnce(String warp, boolean use) {
        if (!main.warpsConfig.contains("warps." + warp)) {
            return false;
        }
        main.warpsConfig.set("warps." + warp + ".useonce", use);

        saveWarps();

        return true;
    }

    public static boolean getUseOnce(String warp) {
        String use = main.warpsConfig.getString("warps." + warp + ".useonce");

        if (use == null) {
            return false;
        }
        return use.equalsIgnoreCase("false");

    }

    public static void addUseOnce(String warp, Player p) {
        List<String> users;
        if (main.usedConfig.contains("used." + warp)) {
            users = main.usedConfig.getStringList("used." + warp);
            if (!users.contains(p.getUniqueId().toString())) {
                users.add(p.getUniqueId().toString());
            }
        } else {
            users = new ArrayList<>();

            users.add(p.getUniqueId().toString());
        }

        main.usedConfig.set("used." + warp, users);

        try {
            main.usedConfig.save(main.used);
        } catch (IOException ex) {
            Logger.getLogger(WarpsManager.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public static boolean canUse(Player p, String warp) {
        if (!main.warpsConfig.contains("warps." + warp + ".useonce")) {
            return true;
        }
        if (main.usedConfig.contains("used." + warp)) {
            List<String> users = main.usedConfig.getStringList("used." + warp);
            return !users.contains(p.getName());
        }
        return true;
    }

    private static void saveWarps() {
        try {
            main.warpsConfig.save(main.warps);
        } catch (IOException ex) {
            Logger.getLogger(WarpsManager.class.getName()).log(Level.SEVERE, "Fehler beim Speichern der Warps", ex);
        }
    }
}
