package de.ferdl9999.MafiWarp.Utils;

import de.ferdl9999.MafiWarp.MafiWarp;
import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.World;

import java.util.HashMap;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

public class WarpCache
        implements Runnable {

    public static HashMap<Location, String> warpCache = new HashMap<>();

    private boolean enabled = true;

    @Override
    public void run() {
        while (enabled) {
            warpCache.clear();
            String warp;
            try {
                for (String s : MafiWarp.getInstance().buttonsConfig.getConfigurationSection("buttons").getKeys(false)) {
                    warp = s;

                    List<String> locs = MafiWarp.getInstance().buttonsConfig.getStringList("buttons." + warp);
                    for (String loc : locs) {
                        String[] loc_tiles = loc.split(",");

                        World world = Bukkit.getWorld(loc_tiles[0]);
                        int x = Integer.parseInt(loc_tiles[1]);
                        int y = Integer.parseInt(loc_tiles[2]);
                        int z = Integer.parseInt(loc_tiles[3]);

                        Location loc_loc = new Location(world, x, y, z);

                        warpCache.put(loc_loc, warp);
                    }
                }
            } catch (Exception ignored) {
            }
            try {
                Thread.sleep(5000L);
            } catch (InterruptedException ex) {
                Logger.getLogger(WarpCache.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }

    public boolean isEnabled() {
        return enabled;
    }

    public void setEnabled(boolean enabled) {
        this.enabled = enabled;
    }
}
