package de.ferdl9999.MafiWarp;

import de.ferdl9999.MafiWarp.Utils.WarpsManager;
import org.bukkit.Location;
import org.bukkit.entity.Player;
import org.bukkit.event.Event;
import org.bukkit.event.HandlerList;

public class WarpEvent extends Event {

    private static final HandlerList handlers = new HandlerList();
    private final String warp;
    private final Player player;
    private final int finalReward;

    public WarpEvent(Player player, String warp, int finalReward) {
        this.warp = warp;
        this.player = player;
        this.finalReward = finalReward;
    }

    public String getName() {
        return this.warp;
    }

    public Location getLocation() {
        return WarpsManager.getLoc(this.warp);
    }

    public Player getPlayer() {
        return this.player;
    }

    public HandlerList getHandlers() {
        return handlers;
    }

    public static HandlerList getHandlerList() {
        return handlers;
    }

    public int getFinalReward() {
        return finalReward;
    }

}
